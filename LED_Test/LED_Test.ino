
/* 
 *  Starting Code for our Camera Rig
 */

//#include "Stepper/src/stepper.h"

#include <Stepper.h>
//#include "multiCameraIrControl.h"



const int stepsPerRevolution = 4096/2;  // change this to fit the number of steps per revolution, for some reason we have to half the actual value

Stepper myStepper(stepsPerRevolution, 7, 8, 9, 10);

 // Globals
 int Button = 11;
 int Buzzer = 12;
 int LED = 13;
 
 int ShortBuzzDur = 20;
 int LongBuzzDur = 1000;
 int BuzzWait = 1000;
 
 int LightDur = 1000;

// Set the PIN types 
void setup(){
  // put your setup code here, to run once:
  pinMode(Button,INPUT);
  pinMode(Buzzer,OUTPUT);
  pinMode(LED,OUTPUT);

  // set the speed at 60 rpm:
  myStepper.setSpeed(6);
}

// Wait for our Button, count down (beep) and then record
void loop(){
  if (digitalRead(Button) == LOW)
  {

    for (int i=0; i<4; i++){
      Buzz(ShortBuzzDur, BuzzWait);
    }
    
    Buzz(LongBuzzDur, 0);    
    Light();
//   myStepper.step(stepsPerRevolution);
//  delay(500);

  }
}

// Buzz for a 
void Buzz(int iDuration, int iWait){
  digitalWrite(Buzzer,HIGH);
  delay(iDuration);
  digitalWrite(Buzzer,LOW);
  delay(iWait);
}


void Light(){
  digitalWrite(LED,HIGH);
  for (int i=0; i<1; i++)
  {
    myStepper.step(stepsPerRevolution); ///2);
    //delay(100);
  }
  //delay(500);
  //delay (LightDur);
  digitalWrite(LED,LOW);
}
